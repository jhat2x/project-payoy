package com.example.apaodevo.basura_juan.Configuration;


/**
 * Created by apaodevo on 11/15/2017.
 */

public class Keys{
     /* Post Data*/
     public static final String TAG_USERNAME = "username";
     public static final String TAG_PASSWORD = "password";
     public  static final String TAG_FIRSTNAME = "fname";
     public static final String TAG_LASTNAME = "lname";
     public static final String TAG_MIDDLE_INITIAL = "minitial";
     public static final String TAG_EMAIL = "email";
     public static final String TAG_USER_IMAGE = "encoded_string";
     public static final String TAG_USER_IMAGE_NAME = "image_name";


    /*JSON Responses*/
     public static final String TAG_SUCCESS = "success";
     public static final String TAG_MESSAGE = "message";
     public static final String TAG_FULLNAME = "fullname";
     public static final String TAG_IMAGE_URL = "image";
     public static final String TAG_USER_EMAIL = "emailAddress";
     public static final String TAG_FNAME = "firstName";
    public static final String TAG_LNAME = "lastName";
    public static final String TAG_MINITIAL = "middleInitial";
    public static final String TAG_PWORD = "password";
    public static final String TAG_ERRORS = "error";

    /* Bin Registration Tags*/
    public static final String TAG_IP_ADDRESS = "ipAddress";
    public static final String TAG_BIN_NAME = "binName";

}
